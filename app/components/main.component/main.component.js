"use strict";
var core_1 = require("@angular/core");
var location_service_1 = require("../../services/location.service");
var tidal_service_1 = require("../../services/tidal.service");
var observableArrayModule = require("data/observable-array");
var MainComponent = (function () {
    function MainComponent(locationService, tidalService) {
        this.locationService = locationService;
        this.tidalService = tidalService;
        this.tidal = new observableArrayModule.ObservableArray({ Country: "Germany", Amount: 15, SecondVal: 14, ThirdVal: 24, Impact: 0, Year: 0 }, { Country: "France", Amount: 13, SecondVal: 23, ThirdVal: 25, Impact: 0, Year: 0 }, { Country: "Bulgaria", Amount: 24, SecondVal: 17, ThirdVal: 23, Impact: 0, Year: 0 }, { Country: "Spain", Amount: 11, SecondVal: 19, ThirdVal: 24, Impact: 0, Year: 0 }, { Country: "USA", Amount: 18, SecondVal: 8, ThirdVal: 21, Impact: 0, Year: 0 });
        this.tides = new observableArrayModule.ObservableArray();
    }
    MainComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.emptyObservable(this.tides);
        this.locationService.isLocationEnabled()
            .then(function (response) {
            console.log("Location Enabled: " + response);
            _this.locationMessage = response;
            _this.locationService.getLocationOnce()
                .then(function (response) {
                _this.latitude = response.latitude;
                _this.longitude = response.longitude;
                //	this.tidalService.getTidesForArea(response.latitude,response.longitude)
                _this.tidalService.getTides(response.latitude, response.longitude)
                    .then(function (result) {
                    //alert("Results "+result);
                    result.forEach(function (element) {
                        _this.tides.push(element);
                        console.log;
                    });
                    //console.log(JSON.stringify(this.tides[0]));
                });
                // .subscribe((result) => {
                // 	alert('Sub '+result);
                // 	// result.forEach(element => {
                // 	// 	this.tides.push(element);
                // 	// })
                // })
            })
                .catch(function (err) {
                alert("there was an error retrieving your location ");
            });
        });
    };
    MainComponent.prototype.reloadData = function () {
    };
    MainComponent.prototype.emptyObservable = function (arr) {
        while (arr.length > 0) {
            arr.pop();
        }
    };
    return MainComponent;
}());
MainComponent = __decorate([
    core_1.Component({
        //selector: 'main',
        //moduleId: module.id,
        templateUrl: './components/main.component/main.component.html',
        //styleUrls: ['./main/main.component.css'],
        providers: [location_service_1.LocationServices, tidal_service_1.TidalServices]
    }),
    __metadata("design:paramtypes", [location_service_1.LocationServices, tidal_service_1.TidalServices])
], MainComponent);
exports.MainComponent = MainComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWluLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsc0NBQWtEO0FBQ2xELG9FQUFtRTtBQUNuRSw4REFBNkQ7QUFFN0QsNkRBQWdFO0FBVWhFLElBQWEsYUFBYTtJQWF6Qix1QkFBb0IsZUFBaUMsRUFBVSxZQUEyQjtRQUF0RSxvQkFBZSxHQUFmLGVBQWUsQ0FBa0I7UUFBVSxpQkFBWSxHQUFaLFlBQVksQ0FBZTtRQVRuRixVQUFLLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQyxlQUFlLENBQ2pELEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsRUFDbkYsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxFQUNsRixFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLEVBQ3BGLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsRUFDakYsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUNwRixDQUFDO1FBQ0ssVUFBSyxHQUFHLElBQUkscUJBQXFCLENBQUMsZUFBZSxFQUFFLENBQUM7SUFJeEQsQ0FBQztJQUVKLGdDQUFRLEdBQVI7UUFBQSxpQkFnQ0M7UUEvQkEsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsRUFBRTthQUN0QyxJQUFJLENBQUMsVUFBQyxRQUFRO1lBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsR0FBQyxRQUFRLENBQUMsQ0FBQztZQUMzQyxLQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQztZQUNoQyxLQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsRUFBRTtpQkFDcEMsSUFBSSxDQUFDLFVBQUMsUUFBUTtnQkFDZCxLQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7Z0JBQ2xDLEtBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQztnQkFDckMsMEVBQTBFO2dCQUN6RSxLQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7cUJBQy9ELElBQUksQ0FBQyxVQUFDLE1BQU07b0JBQ1osMkJBQTJCO29CQUMzQixNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTzt3QkFDckIsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUE7b0JBQ1osQ0FBQyxDQUFDLENBQUE7b0JBQ0YsNkNBQTZDO2dCQUU5QyxDQUFDLENBQUMsQ0FBQTtnQkFDRCwyQkFBMkI7Z0JBQzNCLHlCQUF5QjtnQkFDekIsa0NBQWtDO2dCQUNsQyxpQ0FBaUM7Z0JBQ2pDLFNBQVM7Z0JBQ1QsS0FBSztZQUNQLENBQUMsQ0FBQztpQkFDRCxLQUFLLENBQUMsVUFBQyxHQUFHO2dCQUNWLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1lBQ3ZELENBQUMsQ0FBQyxDQUFBO1FBQ0osQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBQ0Qsa0NBQVUsR0FBVjtJQUVBLENBQUM7SUFDRCx1Q0FBZSxHQUFmLFVBQWdCLEdBQUc7UUFDbEIsT0FBTSxHQUFHLENBQUMsTUFBTSxHQUFDLENBQUMsRUFBRSxDQUFDO1lBQ3BCLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUNYLENBQUM7SUFDRixDQUFDO0lBQ0Ysb0JBQUM7QUFBRCxDQUFDLEFBMURELElBMERDO0FBMURZLGFBQWE7SUFSekIsZ0JBQVMsQ0FBQztRQUNWLG1CQUFtQjtRQUNuQixzQkFBc0I7UUFDdEIsV0FBVyxFQUFFLGlEQUFpRDtRQUM5RCwyQ0FBMkM7UUFDM0MsU0FBUyxFQUFFLENBQUUsbUNBQWdCLEVBQUUsNkJBQWEsQ0FBRTtLQUM5QyxDQUFDO3FDQWVvQyxtQ0FBZ0IsRUFBd0IsNkJBQWE7R0FiOUUsYUFBYSxDQTBEekI7QUExRFksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTG9jYXRpb25TZXJ2aWNlcyB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xvY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgVGlkYWxTZXJ2aWNlcyB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3RpZGFsLnNlcnZpY2UnO1xuXG5pbXBvcnQgb2JzZXJ2YWJsZUFycmF5TW9kdWxlID0gcmVxdWlyZShcImRhdGEvb2JzZXJ2YWJsZS1hcnJheVwiKTtcblxuQENvbXBvbmVudCh7XG5cdC8vc2VsZWN0b3I6ICdtYWluJyxcblx0Ly9tb2R1bGVJZDogbW9kdWxlLmlkLFxuXHR0ZW1wbGF0ZVVybDogJy4vY29tcG9uZW50cy9tYWluLmNvbXBvbmVudC9tYWluLmNvbXBvbmVudC5odG1sJyxcblx0Ly9zdHlsZVVybHM6IFsnLi9tYWluL21haW4uY29tcG9uZW50LmNzcyddLFxuXHRwcm92aWRlcnM6IFsgTG9jYXRpb25TZXJ2aWNlcywgVGlkYWxTZXJ2aWNlcyBdXG59KVxuXG5leHBvcnQgY2xhc3MgTWFpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cdHB1YmxpYyBsb2NhdGlvbk1lc3NhZ2U6IHN0cmluZztcblx0cHVibGljIGxhdGl0dWRlOiBzdHJpbmc7XG5cdHB1YmxpYyBsb25naXR1ZGU6IHN0cmluZztcblx0cHVibGljIHRpZGFsID0gbmV3IG9ic2VydmFibGVBcnJheU1vZHVsZS5PYnNlcnZhYmxlQXJyYXkoXG4gICAgICAgIHsgQ291bnRyeTogXCJHZXJtYW55XCIsIEFtb3VudDogMTUsIFNlY29uZFZhbDogMTQsIFRoaXJkVmFsOiAyNCwgSW1wYWN0OiAwLCBZZWFyOiAwIH0sXG4gICAgICAgIHsgQ291bnRyeTogXCJGcmFuY2VcIiwgQW1vdW50OiAxMywgU2Vjb25kVmFsOiAyMywgVGhpcmRWYWw6IDI1LCBJbXBhY3Q6IDAsIFllYXI6IDAgfSxcbiAgICAgICAgeyBDb3VudHJ5OiBcIkJ1bGdhcmlhXCIsIEFtb3VudDogMjQsIFNlY29uZFZhbDogMTcsIFRoaXJkVmFsOiAyMywgSW1wYWN0OiAwLCBZZWFyOiAwIH0sXG4gICAgICAgIHsgQ291bnRyeTogXCJTcGFpblwiLCBBbW91bnQ6IDExLCBTZWNvbmRWYWw6IDE5LCBUaGlyZFZhbDogMjQsIEltcGFjdDogMCwgWWVhcjogMCB9LFxuICAgICAgICB7IENvdW50cnk6IFwiVVNBXCIsIEFtb3VudDogMTgsIFNlY29uZFZhbDogOCwgVGhpcmRWYWw6IDIxLCBJbXBhY3Q6IDAsIFllYXI6IDAgfVxuXHQpO1xuXHRwdWJsaWMgdGlkZXMgPSBuZXcgb2JzZXJ2YWJsZUFycmF5TW9kdWxlLk9ic2VydmFibGVBcnJheSgpO1xuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgbG9jYXRpb25TZXJ2aWNlOiBMb2NhdGlvblNlcnZpY2VzLCBwcml2YXRlIHRpZGFsU2VydmljZTogVGlkYWxTZXJ2aWNlcykge1xuICAgICAgICBcbiAgICB9XG5cblx0bmdPbkluaXQoKSB7IFxuXHRcdHRoaXMuZW1wdHlPYnNlcnZhYmxlKHRoaXMudGlkZXMpO1xuXHRcdHRoaXMubG9jYXRpb25TZXJ2aWNlLmlzTG9jYXRpb25FbmFibGVkKClcblx0XHRcdC50aGVuKChyZXNwb25zZSkgPT4ge1xuXHRcdFx0XHRjb25zb2xlLmxvZyhcIkxvY2F0aW9uIEVuYWJsZWQ6IFwiK3Jlc3BvbnNlKTtcblx0XHRcdFx0dGhpcy5sb2NhdGlvbk1lc3NhZ2UgPSByZXNwb25zZTtcblx0XHRcdFx0dGhpcy5sb2NhdGlvblNlcnZpY2UuZ2V0TG9jYXRpb25PbmNlKClcblx0XHRcdFx0XHQudGhlbigocmVzcG9uc2UpID0+IHtcblx0XHRcdFx0XHRcdHRoaXMubGF0aXR1ZGUgPSByZXNwb25zZS5sYXRpdHVkZTtcblx0XHRcdFx0XHRcdHRoaXMubG9uZ2l0dWRlID0gcmVzcG9uc2UubG9uZ2l0dWRlO1xuXHRcdFx0XHRcdC8vXHR0aGlzLnRpZGFsU2VydmljZS5nZXRUaWRlc0ZvckFyZWEocmVzcG9uc2UubGF0aXR1ZGUscmVzcG9uc2UubG9uZ2l0dWRlKVxuXHRcdFx0XHRcdFx0dGhpcy50aWRhbFNlcnZpY2UuZ2V0VGlkZXMocmVzcG9uc2UubGF0aXR1ZGUscmVzcG9uc2UubG9uZ2l0dWRlKVxuXHRcdFx0XHRcdFx0LnRoZW4oKHJlc3VsdCkgPT4ge1xuXHRcdFx0XHRcdFx0XHQvL2FsZXJ0KFwiUmVzdWx0cyBcIityZXN1bHQpO1xuXHRcdFx0XHRcdFx0XHRyZXN1bHQuZm9yRWFjaChlbGVtZW50ID0+IHtcblx0XHRcdFx0XHRcdFx0XHR0aGlzLnRpZGVzLnB1c2goZWxlbWVudCk7XG5cdFx0XHRcdFx0XHRcdFx0Y29uc29sZS5sb2dcblx0XHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHRcdFx0Ly9jb25zb2xlLmxvZyhKU09OLnN0cmluZ2lmeSh0aGlzLnRpZGVzWzBdKSk7XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0fSlcblx0XHRcdFx0XHRcdFx0Ly8gLnN1YnNjcmliZSgocmVzdWx0KSA9PiB7XG5cdFx0XHRcdFx0XHRcdC8vIFx0YWxlcnQoJ1N1YiAnK3Jlc3VsdCk7XG5cdFx0XHRcdFx0XHRcdC8vIFx0Ly8gcmVzdWx0LmZvckVhY2goZWxlbWVudCA9PiB7XG5cdFx0XHRcdFx0XHRcdC8vIFx0Ly8gXHR0aGlzLnRpZGVzLnB1c2goZWxlbWVudCk7XG5cdFx0XHRcdFx0XHRcdC8vIFx0Ly8gfSlcblx0XHRcdFx0XHRcdFx0Ly8gfSlcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5jYXRjaCgoZXJyKSA9PiB7XG5cdFx0XHRcdFx0XHRhbGVydChcInRoZXJlIHdhcyBhbiBlcnJvciByZXRyaWV2aW5nIHlvdXIgbG9jYXRpb24gXCIpO1xuXHRcdFx0XHRcdH0pXG5cdFx0XHR9KVxuXHR9XG5cdHJlbG9hZERhdGEoKSB7XG5cblx0fVxuXHRlbXB0eU9ic2VydmFibGUoYXJyKSB7XG5cdFx0d2hpbGUoYXJyLmxlbmd0aD4wKSB7XG5cdFx0XHRhcnIucG9wKCk7XG5cdFx0fVxuXHR9XG59Il19