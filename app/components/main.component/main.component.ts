import { Component, OnInit } from '@angular/core';
import { LocationServices } from '../../services/location.service';
import { TidalServices } from '../../services/tidal.service';

import observableArrayModule = require("data/observable-array");

@Component({
	//selector: 'main',
	//moduleId: module.id,
	templateUrl: './components/main.component/main.component.html',
	//styleUrls: ['./main/main.component.css'],
	providers: [ LocationServices, TidalServices ]
})

export class MainComponent implements OnInit {
	public locationMessage: string;
	public latitude: string;
	public longitude: string;
	public tidal = new observableArrayModule.ObservableArray(
        { Country: "Germany", Amount: 15, SecondVal: 14, ThirdVal: 24, Impact: 0, Year: 0 },
        { Country: "France", Amount: 13, SecondVal: 23, ThirdVal: 25, Impact: 0, Year: 0 },
        { Country: "Bulgaria", Amount: 24, SecondVal: 17, ThirdVal: 23, Impact: 0, Year: 0 },
        { Country: "Spain", Amount: 11, SecondVal: 19, ThirdVal: 24, Impact: 0, Year: 0 },
        { Country: "USA", Amount: 18, SecondVal: 8, ThirdVal: 21, Impact: 0, Year: 0 }
	);
	public tides = new observableArrayModule.ObservableArray();

	constructor(private locationService: LocationServices, private tidalService: TidalServices) {
        
    }

	ngOnInit() { 
		this.emptyObservable(this.tides);
		this.locationService.isLocationEnabled()
			.then((response) => {
				console.log("Location Enabled: "+response);
				this.locationMessage = response;
				this.locationService.getLocationOnce()
					.then((response) => {
						this.latitude = response.latitude;
						this.longitude = response.longitude;
					//	this.tidalService.getTidesForArea(response.latitude,response.longitude)
						this.tidalService.getTides(response.latitude,response.longitude)
						.then((result) => {
							//alert("Results "+result);
							result.forEach(element => {
								this.tides.push(element);
								console.log
							})
							//console.log(JSON.stringify(this.tides[0]));
							
						})
							// .subscribe((result) => {
							// 	alert('Sub '+result);
							// 	// result.forEach(element => {
							// 	// 	this.tides.push(element);
							// 	// })
							// })
					})
					.catch((err) => {
						alert("there was an error retrieving your location ");
					})
			})
	}
	reloadData() {

	}
	emptyObservable(arr) {
		while(arr.length>0) {
			arr.pop();
		}
	}
}