"use strict";
var testing_1 = require("@angular/core/testing");
var main_component_1 = require("./main.component");
describe('a main component', function () {
    var component;
    // register all needed dependencies
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [
                main_component_1.MainComponent
            ]
        });
    });
    // instantiation through framework injection
    beforeEach(testing_1.inject([main_component_1.MainComponent], function (MainComponent) {
        component = MainComponent;
    }));
    it('should have an instance', function () {
        expect(component).toBeDefined();
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5jb21wb25lbnQuc3BlYy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1haW4uY29tcG9uZW50LnNwZWMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLGlEQUF3RDtBQUV4RCxtREFBaUQ7QUFFakQsUUFBUSxDQUFDLGtCQUFrQixFQUFFO0lBQzVCLElBQUksU0FBd0IsQ0FBQztJQUU3QixtQ0FBbUM7SUFDbkMsVUFBVSxDQUFDO1FBQ1YsaUJBQU8sQ0FBQyxzQkFBc0IsQ0FBQztZQUM5QixTQUFTLEVBQUU7Z0JBQ1YsOEJBQWE7YUFDYjtTQUNELENBQUMsQ0FBQztJQUNKLENBQUMsQ0FBQyxDQUFDO0lBRUgsNENBQTRDO0lBQzVDLFVBQVUsQ0FBQyxnQkFBTSxDQUFDLENBQUMsOEJBQWEsQ0FBQyxFQUFFLFVBQUMsYUFBYTtRQUNoRCxTQUFTLEdBQUcsYUFBYSxDQUFDO0lBQzNCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFSixFQUFFLENBQUMseUJBQXlCLEVBQUU7UUFDN0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2pDLENBQUMsQ0FBQyxDQUFDO0FBQ0osQ0FBQyxDQUFDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUZXN0QmVkLCBpbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnO1xuaW1wb3J0IHt9IGZyb20gJ2phc21pbmUnO1xuaW1wb3J0IHsgTWFpbkNvbXBvbmVudCB9IGZyb20gJy4vbWFpbi5jb21wb25lbnQnO1xuXG5kZXNjcmliZSgnYSBtYWluIGNvbXBvbmVudCcsICgpID0+IHtcblx0bGV0IGNvbXBvbmVudDogTWFpbkNvbXBvbmVudDtcblxuXHQvLyByZWdpc3RlciBhbGwgbmVlZGVkIGRlcGVuZGVuY2llc1xuXHRiZWZvcmVFYWNoKCgpID0+IHtcblx0XHRUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuXHRcdFx0cHJvdmlkZXJzOiBbXG5cdFx0XHRcdE1haW5Db21wb25lbnRcblx0XHRcdF1cblx0XHR9KTtcblx0fSk7XG5cblx0Ly8gaW5zdGFudGlhdGlvbiB0aHJvdWdoIGZyYW1ld29yayBpbmplY3Rpb25cblx0YmVmb3JlRWFjaChpbmplY3QoW01haW5Db21wb25lbnRdLCAoTWFpbkNvbXBvbmVudCkgPT4ge1xuXHRcdGNvbXBvbmVudCA9IE1haW5Db21wb25lbnQ7XG5cdH0pKTtcblxuXHRpdCgnc2hvdWxkIGhhdmUgYW4gaW5zdGFuY2UnLCAoKSA9PiB7XG5cdFx0ZXhwZWN0KGNvbXBvbmVudCkudG9CZURlZmluZWQoKTtcblx0fSk7XG59KTsiXX0=