import { MainComponent } from "./components/main.component/main.component";

export const routes = [
    { path: "", redirectTo: "/main", pathMatch: "full" },
    { path: "main", component: MainComponent }
];

export const navigatableComponents = [
    MainComponent
]