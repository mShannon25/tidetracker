import { Injectable,NgZone } from "@angular/core";
import { Location, getCurrentLocation, isEnabled, distance, enableLocationRequest } from "nativescript-geolocation";
import { Http, HttpModule, Headers, Response, ResponseOptions, RequestOptions } from "@angular/http";
import { Observable, BehaviorSubject } from "rxjs/Rx";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/catch";

//let humanizeDistance = require("humanize-distance");

@Injectable()

export class LocationServices {

    public distanceResult: string = "0";
    public distance: number = 0;
    public index: number = 0;

    public latitude: number;
    public longitude: number;

    public startpointLongitude: number = 42.696552;
    public startpointLatitude: number = 23.32601;
    public endpointLongitude: number = 40.71448;
    public endpointLatitude: number = -74.00598;

    constructor(private http: Http, private zone: NgZone){
        enableLocationRequest(true);
    }

  	public isLocationEnabled(): Promise<any> {
        return new Promise((resolve, reject) => {
          let isEnabledProperty = isEnabled();
          let message;
          if (isEnabledProperty) {
            resolve(isEnabledProperty);
              message = "Location services are available";
          }else{
            reject(isEnabledProperty);
            enableLocationRequest(true)
            message = "Location services are not available";
          }
          //alert(message);
        })
    }

    public getDistance() {
        // >> get-distance
        let startLocation: Location = new Location();
        startLocation.longitude = this.startpointLongitude;
        startLocation.latitude = this.startpointLatitude;

        let endLocation: Location = new Location();
        endLocation.longitude = this.endpointLongitude;
        endLocation.latitude = this.endpointLatitude;
        this.distance = distance(startLocation, endLocation);
        // << get-distance

        console.log("distance - " + this.distance);
        this.distanceResult = (this.distance * 0.001).toFixed(3);
    }

    public getLocationOnce(): Promise<any> {
        return new Promise((resolve, reject) => {
          getCurrentLocation({ timeout: 20000 })
              .then(location => {
                  console.log("Location received " + JSON.stringify(location));
                  this.latitude = location.latitude;
                  this.longitude = location.longitude;
                  resolve(location)
              }).catch(error => {
                reject(error);
                  console.log("Location error received: " + error);
                  alert("Location error received: " + error);
              });
          // << get-current-location
        })
    }
}
