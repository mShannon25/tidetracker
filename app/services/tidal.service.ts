import { Injectable,NgZone } from "@angular/core";
import { Http, HttpModule, Headers, Response, ResponseOptions, RequestOptions } from "@angular/http";
import { Observable, BehaviorSubject } from "rxjs/Rx";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/catch";

@Injectable()
export class TidalServices {
    private tideKey: string = "8f4ca8c1-8846-43c6-a60a-a16c0d996d31";
    private tides: Array<any> = [];

    constructor(private http: Http, private zone: NgZone){}

    getTides(lat: string, lon: string) {
        let url = "https://www.worldtides.info/api?heights&lat="+lat+"&lon="+lon+"&step=3600&key="+this.tideKey;
        return fetch(url,
        {method:"GET"})
           .then((response) => {
             return response.json();
           },(e) =>{
                alert("ERROR "+e);
           }).then((data) => {
              
                data.heights.forEach((item) => {
                //     //console.log("ID "+id);   
                //    // let result = (<any>Object).assign({id: id}, data[id]);
                   console.log("\nTide: "+item["height"]+"\nDate: "+item["date"]);
                     this.tides.push(item);
                 })
                  console.log("\nStation: "+data["station"])
                 return this.tides;
           })
    }
    // Cant get this RXJS Rest http call to work so went with the promise version above
    getTidesForArea(lat: string, lon: string){
        let headers = this.getHeaders();
        headers.append("Accept", "application/json");
        let url = "https://www.worldtides.info/api?heights&lat="+lat+"&lon="+lon+"&start=1412272800000&length=432000000&step=18000000&key="+this.tideKey;
       // headers.append("Authorization", "Bearer " + this.tideKey);
        return this.http.get(url
// return this.http.get("https://pod.opendatasoft.com/api/records/1.0/search/?dataset=pod_gtin&q=0072030014210&lang=en&facet=gpc_s_nm&facet=brand_nm&facet=owner_nm&facet=gln_nm&facet=prefix_nm",
          //  {headers: headers}
        )
           // .map(result => JSON.stringify(result["_body"]))
          // .map(result => JSON.parse(result["url"]).json())
           .map(result => result.json())
            .catch(this.handleErrors)
            .subscribe((result) => {
               console.log(JSON.stringify(result));
         
               // alert("DATAT "+data);
                //  data.heights.forEach((item) => {
                //     //console.log("ID "+id);   
                //    // let result = (<any>Object).assign({id: id}, data[id]);
                // //   console.log("Result: "+result.product);
                //     this.tides.push(item);
                // })
            // console.log("Produc length "+this.productList.length);
           // return data;
        })
    }
    private getHeaders() {
        let headers = new Headers();
        //let token = this.getToken();
        headers.append("Content-Type", "application/json");
        headers.append("Access-Control-Allow-Origin", "*");
    // headers.append("Authorization", "Bearer " + "eyJhbGciOiJSUzI1NiIsImtpZCI6IjFlOTc0OWNkMjBlOTkzOTlmNmExYTQ2ZGNhMWVjMWMwYWQxNTFkMzIifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZnJlc2huZXNzLTE2MDExNCIsImF1ZCI6ImZyZXNobmVzcy0xNjAxMTQiLCJhdXRoX3RpbWUiOjE0ODk0Mjk2MDEsInVzZXJfaWQiOiJHT2RBT0dwR2k1Y1RraFFwUXF4c01JYnBFcngxIiwic3ViIjoiR09kQU9HcEdpNWNUa2hRcFFxeHNNSWJwRXJ4MSIsImlhdCI6MTQ4OTQyOTYwMSwiZXhwIjoxNDg5NDMzMjAxLCJlbWFpbCI6Im1rczEyMjVAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsibWtzMTIyNUBnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.Wr_9FPdnHw9ScN5SmmN0osilnjV1PaB295Ptb2NxrL73mGrOYe30SCO8lscxW47-w_m6NzQluPY_tp5JL7C0JmGiUkMqZCahIf_QgJ_zJlhOt30KQqh94v--urL5pZWLXvUcXgEgCSiItv6DaQrsh0KsdspseB3CCDafw-pzck1Lnif8aSy71JyYhYc2LrAnc4BVOug8zD8MSioRR19PV2mieXPW5z9eaME3NEdtAkhAdjIRGGhVyPypFMD7r_kuZJRFeyjTVBDPLLsq7MFK3aPblINvHK55JTuRFlFqpQzc3JOGHhFhwGgzP26MdVjvLFS90Y_v67YCcOshRtDDNg");
    //  console.log(":"+JSON.stringify(headers));
    // console.log(this._token);
        return headers;
    }
    private handleErrors(error: Response) {
        alert("ERROR: "+error);
        return Observable.throw(error);
    }
}