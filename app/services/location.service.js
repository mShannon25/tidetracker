"use strict";
var core_1 = require("@angular/core");
var nativescript_geolocation_1 = require("nativescript-geolocation");
var http_1 = require("@angular/http");
require("rxjs/add/operator/do");
require("rxjs/add/operator/map");
require("rxjs/add/observable/throw");
require("rxjs/add/operator/catch");
//let humanizeDistance = require("humanize-distance");
var LocationServices = (function () {
    function LocationServices(http, zone) {
        this.http = http;
        this.zone = zone;
        this.distanceResult = "0";
        this.distance = 0;
        this.index = 0;
        this.startpointLongitude = 42.696552;
        this.startpointLatitude = 23.32601;
        this.endpointLongitude = 40.71448;
        this.endpointLatitude = -74.00598;
        nativescript_geolocation_1.enableLocationRequest(true);
    }
    LocationServices.prototype.isLocationEnabled = function () {
        return new Promise(function (resolve, reject) {
            var isEnabledProperty = nativescript_geolocation_1.isEnabled();
            var message;
            if (isEnabledProperty) {
                resolve(isEnabledProperty);
                message = "Location services are available";
            }
            else {
                reject(isEnabledProperty);
                nativescript_geolocation_1.enableLocationRequest(true);
                message = "Location services are not available";
            }
            //alert(message);
        });
    };
    LocationServices.prototype.getDistance = function () {
        // >> get-distance
        var startLocation = new nativescript_geolocation_1.Location();
        startLocation.longitude = this.startpointLongitude;
        startLocation.latitude = this.startpointLatitude;
        var endLocation = new nativescript_geolocation_1.Location();
        endLocation.longitude = this.endpointLongitude;
        endLocation.latitude = this.endpointLatitude;
        this.distance = nativescript_geolocation_1.distance(startLocation, endLocation);
        // << get-distance
        console.log("distance - " + this.distance);
        this.distanceResult = (this.distance * 0.001).toFixed(3);
    };
    LocationServices.prototype.getLocationOnce = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            nativescript_geolocation_1.getCurrentLocation({ timeout: 20000 })
                .then(function (location) {
                console.log("Location received " + JSON.stringify(location));
                _this.latitude = location.latitude;
                _this.longitude = location.longitude;
                resolve(location);
            }).catch(function (error) {
                reject(error);
                console.log("Location error received: " + error);
                alert("Location error received: " + error);
            });
            // << get-current-location
        });
    };
    return LocationServices;
}());
LocationServices = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, core_1.NgZone])
], LocationServices);
exports.LocationServices = LocationServices;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxvY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHNDQUFrRDtBQUNsRCxxRUFBb0g7QUFDcEgsc0NBQXFHO0FBRXJHLGdDQUE4QjtBQUM5QixpQ0FBK0I7QUFDL0IscUNBQW1DO0FBQ25DLG1DQUFpQztBQUVqQyxzREFBc0Q7QUFJdEQsSUFBYSxnQkFBZ0I7SUFjekIsMEJBQW9CLElBQVUsRUFBVSxJQUFZO1FBQWhDLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFRO1FBWjdDLG1CQUFjLEdBQVcsR0FBRyxDQUFDO1FBQzdCLGFBQVEsR0FBVyxDQUFDLENBQUM7UUFDckIsVUFBSyxHQUFXLENBQUMsQ0FBQztRQUtsQix3QkFBbUIsR0FBVyxTQUFTLENBQUM7UUFDeEMsdUJBQWtCLEdBQVcsUUFBUSxDQUFDO1FBQ3RDLHNCQUFpQixHQUFXLFFBQVEsQ0FBQztRQUNyQyxxQkFBZ0IsR0FBVyxDQUFDLFFBQVEsQ0FBQztRQUd4QyxnREFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUssNENBQWlCLEdBQXhCO1FBQ0ssTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDakMsSUFBSSxpQkFBaUIsR0FBRyxvQ0FBUyxFQUFFLENBQUM7WUFDcEMsSUFBSSxPQUFPLENBQUM7WUFDWixFQUFFLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RCLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUN6QixPQUFPLEdBQUcsaUNBQWlDLENBQUM7WUFDaEQsQ0FBQztZQUFBLElBQUksQ0FBQSxDQUFDO2dCQUNKLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUMxQixnREFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQTtnQkFDM0IsT0FBTyxHQUFHLHFDQUFxQyxDQUFDO1lBQ2xELENBQUM7WUFDRCxpQkFBaUI7UUFDbkIsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRU0sc0NBQVcsR0FBbEI7UUFDSSxrQkFBa0I7UUFDbEIsSUFBSSxhQUFhLEdBQWEsSUFBSSxtQ0FBUSxFQUFFLENBQUM7UUFDN0MsYUFBYSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7UUFDbkQsYUFBYSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFFakQsSUFBSSxXQUFXLEdBQWEsSUFBSSxtQ0FBUSxFQUFFLENBQUM7UUFDM0MsV0FBVyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDL0MsV0FBVyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDN0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxtQ0FBUSxDQUFDLGFBQWEsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUNyRCxrQkFBa0I7UUFFbEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRU0sMENBQWUsR0FBdEI7UUFBQSxpQkFlQztRQWRHLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ2pDLDZDQUFrQixDQUFDLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxDQUFDO2lCQUNqQyxJQUFJLENBQUMsVUFBQSxRQUFRO2dCQUNWLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUM3RCxLQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7Z0JBQ2xDLEtBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQztnQkFDcEMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFBO1lBQ3JCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEtBQUs7Z0JBQ1osTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLEdBQUcsS0FBSyxDQUFDLENBQUM7Z0JBQ2pELEtBQUssQ0FBQywyQkFBMkIsR0FBRyxLQUFLLENBQUMsQ0FBQztZQUMvQyxDQUFDLENBQUMsQ0FBQztZQUNQLDBCQUEwQjtRQUM1QixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFDTCx1QkFBQztBQUFELENBQUMsQUFsRUQsSUFrRUM7QUFsRVksZ0JBQWdCO0lBRjVCLGlCQUFVLEVBQUU7cUNBZ0JpQixXQUFJLEVBQWdCLGFBQU07R0FkM0MsZ0JBQWdCLENBa0U1QjtBQWxFWSw0Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLE5nWm9uZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBMb2NhdGlvbiwgZ2V0Q3VycmVudExvY2F0aW9uLCBpc0VuYWJsZWQsIGRpc3RhbmNlLCBlbmFibGVMb2NhdGlvblJlcXVlc3QgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWdlb2xvY2F0aW9uXCI7XG5pbXBvcnQgeyBIdHRwLCBIdHRwTW9kdWxlLCBIZWFkZXJzLCBSZXNwb25zZSwgUmVzcG9uc2VPcHRpb25zLCBSZXF1ZXN0T3B0aW9ucyB9IGZyb20gXCJAYW5ndWxhci9odHRwXCI7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBCZWhhdmlvclN1YmplY3QgfSBmcm9tIFwicnhqcy9SeFwiO1xuaW1wb3J0IFwicnhqcy9hZGQvb3BlcmF0b3IvZG9cIjtcbmltcG9ydCBcInJ4anMvYWRkL29wZXJhdG9yL21hcFwiO1xuaW1wb3J0IFwicnhqcy9hZGQvb2JzZXJ2YWJsZS90aHJvd1wiO1xuaW1wb3J0IFwicnhqcy9hZGQvb3BlcmF0b3IvY2F0Y2hcIjtcblxuLy9sZXQgaHVtYW5pemVEaXN0YW5jZSA9IHJlcXVpcmUoXCJodW1hbml6ZS1kaXN0YW5jZVwiKTtcblxuQEluamVjdGFibGUoKVxuXG5leHBvcnQgY2xhc3MgTG9jYXRpb25TZXJ2aWNlcyB7XG5cbiAgICBwdWJsaWMgZGlzdGFuY2VSZXN1bHQ6IHN0cmluZyA9IFwiMFwiO1xuICAgIHB1YmxpYyBkaXN0YW5jZTogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgaW5kZXg6IG51bWJlciA9IDA7XG5cbiAgICBwdWJsaWMgbGF0aXR1ZGU6IG51bWJlcjtcbiAgICBwdWJsaWMgbG9uZ2l0dWRlOiBudW1iZXI7XG5cbiAgICBwdWJsaWMgc3RhcnRwb2ludExvbmdpdHVkZTogbnVtYmVyID0gNDIuNjk2NTUyO1xuICAgIHB1YmxpYyBzdGFydHBvaW50TGF0aXR1ZGU6IG51bWJlciA9IDIzLjMyNjAxO1xuICAgIHB1YmxpYyBlbmRwb2ludExvbmdpdHVkZTogbnVtYmVyID0gNDAuNzE0NDg7XG4gICAgcHVibGljIGVuZHBvaW50TGF0aXR1ZGU6IG51bWJlciA9IC03NC4wMDU5ODtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cCwgcHJpdmF0ZSB6b25lOiBOZ1pvbmUpe1xuICAgICAgICBlbmFibGVMb2NhdGlvblJlcXVlc3QodHJ1ZSk7XG4gICAgfVxuXG4gIFx0cHVibGljIGlzTG9jYXRpb25FbmFibGVkKCk6IFByb21pc2U8YW55PiB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgbGV0IGlzRW5hYmxlZFByb3BlcnR5ID0gaXNFbmFibGVkKCk7XG4gICAgICAgICAgbGV0IG1lc3NhZ2U7XG4gICAgICAgICAgaWYgKGlzRW5hYmxlZFByb3BlcnR5KSB7XG4gICAgICAgICAgICByZXNvbHZlKGlzRW5hYmxlZFByb3BlcnR5KTtcbiAgICAgICAgICAgICAgbWVzc2FnZSA9IFwiTG9jYXRpb24gc2VydmljZXMgYXJlIGF2YWlsYWJsZVwiO1xuICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgcmVqZWN0KGlzRW5hYmxlZFByb3BlcnR5KTtcbiAgICAgICAgICAgIGVuYWJsZUxvY2F0aW9uUmVxdWVzdCh0cnVlKVxuICAgICAgICAgICAgbWVzc2FnZSA9IFwiTG9jYXRpb24gc2VydmljZXMgYXJlIG5vdCBhdmFpbGFibGVcIjtcbiAgICAgICAgICB9XG4gICAgICAgICAgLy9hbGVydChtZXNzYWdlKTtcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0RGlzdGFuY2UoKSB7XG4gICAgICAgIC8vID4+IGdldC1kaXN0YW5jZVxuICAgICAgICBsZXQgc3RhcnRMb2NhdGlvbjogTG9jYXRpb24gPSBuZXcgTG9jYXRpb24oKTtcbiAgICAgICAgc3RhcnRMb2NhdGlvbi5sb25naXR1ZGUgPSB0aGlzLnN0YXJ0cG9pbnRMb25naXR1ZGU7XG4gICAgICAgIHN0YXJ0TG9jYXRpb24ubGF0aXR1ZGUgPSB0aGlzLnN0YXJ0cG9pbnRMYXRpdHVkZTtcblxuICAgICAgICBsZXQgZW5kTG9jYXRpb246IExvY2F0aW9uID0gbmV3IExvY2F0aW9uKCk7XG4gICAgICAgIGVuZExvY2F0aW9uLmxvbmdpdHVkZSA9IHRoaXMuZW5kcG9pbnRMb25naXR1ZGU7XG4gICAgICAgIGVuZExvY2F0aW9uLmxhdGl0dWRlID0gdGhpcy5lbmRwb2ludExhdGl0dWRlO1xuICAgICAgICB0aGlzLmRpc3RhbmNlID0gZGlzdGFuY2Uoc3RhcnRMb2NhdGlvbiwgZW5kTG9jYXRpb24pO1xuICAgICAgICAvLyA8PCBnZXQtZGlzdGFuY2VcblxuICAgICAgICBjb25zb2xlLmxvZyhcImRpc3RhbmNlIC0gXCIgKyB0aGlzLmRpc3RhbmNlKTtcbiAgICAgICAgdGhpcy5kaXN0YW5jZVJlc3VsdCA9ICh0aGlzLmRpc3RhbmNlICogMC4wMDEpLnRvRml4ZWQoMyk7XG4gICAgfVxuXG4gICAgcHVibGljIGdldExvY2F0aW9uT25jZSgpOiBQcm9taXNlPGFueT4ge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgIGdldEN1cnJlbnRMb2NhdGlvbih7IHRpbWVvdXQ6IDIwMDAwIH0pXG4gICAgICAgICAgICAgIC50aGVuKGxvY2F0aW9uID0+IHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTG9jYXRpb24gcmVjZWl2ZWQgXCIgKyBKU09OLnN0cmluZ2lmeShsb2NhdGlvbikpO1xuICAgICAgICAgICAgICAgICAgdGhpcy5sYXRpdHVkZSA9IGxvY2F0aW9uLmxhdGl0dWRlO1xuICAgICAgICAgICAgICAgICAgdGhpcy5sb25naXR1ZGUgPSBsb2NhdGlvbi5sb25naXR1ZGU7XG4gICAgICAgICAgICAgICAgICByZXNvbHZlKGxvY2F0aW9uKVxuICAgICAgICAgICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICAgICAgICAgcmVqZWN0KGVycm9yKTtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTG9jYXRpb24gZXJyb3IgcmVjZWl2ZWQ6IFwiICsgZXJyb3IpO1xuICAgICAgICAgICAgICAgICAgYWxlcnQoXCJMb2NhdGlvbiBlcnJvciByZWNlaXZlZDogXCIgKyBlcnJvcik7XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgIC8vIDw8IGdldC1jdXJyZW50LWxvY2F0aW9uXG4gICAgICAgIH0pXG4gICAgfVxufVxuIl19